
toUpperCase()
{
    result+=`echo $1 | tr '[a-z]' '[A-Z]'`;
}

charToLeet()
{
    RANDOM=$$
    char=$1;
    if [ "$char" = "a" ] || [ "$char" = "A" ] || [ "$char" = "ä" ] || [ "$char" = "Ä" ]
    then
        result+="4";
    elif [ "$char" = "l" ] || [ "$char" = "L" ] || [ "$char" = "i" ] || [ "$char" = "I" ]
    then
        result+="1";
    elif [ "$char" = "e" ] || [ "$char" = "E" ]
    then
        result+="3";
    elif [ "$char" = "s" ] || [ "$char" = "S" ]
    then
        result+="5";
    elif [ "$char" = "t" ] || [ "$char" = "T" ]
    then
        result+="7";
    elif [ "$char" = "o" ] || [ "$char" = "O" ] || [ "$char" = "ö" ] || [ "$char" = "Ö" ]
    then
        result+="0";
    elif [ "$char" = "g" ] || [ "$char" = "G" ]
    then
        result+="6";
    elif [ "$char" = "b" ] || [ "$char" = "B" ]
    then
        result+="8";
    elif [ "$char" = "z" ] || [ "$char" = "Z" ]
    then
        result+="2";
    elif [ `expr $RANDOM % 2` -eq 0 ]
    then
        toUpperCase $char;
    else
        result+=$char;
    fi
}

result="";
for arg in "$@" 
do
    word=$arg;
    for (( i = 0; i < ${#word}; i++ )); 
    do
        charToLeet "${word:$i:1}";
    done
    result+=" ";
done

echo $result